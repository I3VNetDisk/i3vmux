package i3vmux

import (
	"fmt"
	"io"

	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/i3vmux/mux"
)

// staticStopped returns 'true' if Close has been called on the I3vMux before.
func (sm *I3vMux) staticStopped() bool {
	select {
	case <-sm.stopped:
		return true
	default:
		return false
	}
}

// NewListener returns a new listener for a given subscriber name.
func (sm *I3vMux) NewListener(subscriber string, handler Handler) error {
	if sm.staticStopped() {
		return errors.New("I3vMux has already been closed")
	}
	sm.mu.Lock()
	defer sm.mu.Unlock()
	// Check if handler already exists.
	_, exists := sm.handlers[subscriber]
	if exists {
		return fmt.Errorf("handler for subscriber %v already registered", subscriber)
	}
	// Register the handler.
	sm.handlers[subscriber] = handler
	return nil
}

// CloseListener will close a previously registered listener, causing incoming
// streams for that listener to be dropped with an error.
func (sm *I3vMux) CloseListener(subscriber string) error {
	sm.mu.Lock()
	defer sm.mu.Unlock()
	_, exists := sm.handlers[subscriber]
	if !exists {
		return fmt.Errorf("handler for subscriber %v doesn't exist", subscriber)
	}
	// Remove handler.
	delete(sm.handlers, subscriber)
	return nil
}

// threadedAccept is spawned for every open connection wrapped in a multiplexer.
// It will constantly accept streams on that multiplexer, discard the ones that
// refer to unknown subscribers and forward the other ones to the corresponding
// listener.
func (sm *I3vMux) threadedAccept(mux *mux.Mux) {
	// Start accepting streams.
	for {
		select {
		case <-mux.StopChan():
			return // mux closed
		default:
		}
		// Accept a stream.
		stream, err := mux.AcceptStream()
		if errors.Contains(err, io.EOF) {
			return
		} else if err != nil {
			sm.staticLog.Print("I3vMux: failed to accept stream", err)
			continue
		}
		// Read the subscriber.
		subscriber, err := readSubscriberRequest(stream)
		if err != nil {
			sm.staticLog.Print("I3vMux: failed to read subscriber", errors.Compose(err, stream.Close()))
			continue
		}
		// Check if a handler exists for the subscriber.
		sm.mu.Lock()
		handler, exists := sm.handlers[subscriber]
		sm.mu.Unlock()
		if !exists {
			err = writeSubscriberResponse(stream, errUnknownSubscriber)
			sm.staticLog.Print("I3vMux: unknown subscriber", subscriber, errors.Compose(stream.Close()))
			continue
		}
		// Send the 'ok' response.
		if err := writeSubscriberResponse(stream, nil); err != nil {
			sm.staticLog.Print("I3vMux: failed to send subscriber response", errors.Compose(err, stream.Close()))
			continue
		}
		// Call the handler in a separate goroutine.
		sm.wg.Add(1)
		go func() {
			handler(stream)
			sm.wg.Done()
		}()
	}
}
