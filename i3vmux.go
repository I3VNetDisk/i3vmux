package i3vmux

import (
	"encoding/binary"
	"math"
	"net"
	"sync"

	"gitlab.com/I3VNetDisk/I3v/crypto"
	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/fastrand"
	"gitlab.com/I3VNetDisk/i3vmux/mux"
)

// TODO: Some features/tests that came to mind which we need
// - more testing around the appseed
// - auto connect on i3vmux creation
// - periodically send keepalive
type (
	// I3vMux needs a docstring
	I3vMux struct {
		handlers       map[string]Handler   // registered handlers for incoming connections
		outgoingMuxs   map[string]*mux.Mux  // maps addresses of outgoing connection to the mux that is wrapping them
		muxs           map[appSeed]*mux.Mux // all incoming and outgoing muxs
		muxSet         map[*mux.Mux]muxInfo // control set to confirm that every mux only has a single seed associated to it
		staticAppSeed  appSeed              // random appSeed to uniquely identify application
		staticListener net.Listener         // listener used to listen for incoming I3vMux requests
		staticPubKey   mux.ED25519PublicKey
		staticPrivKey  mux.ED25519SecretKey

		// utilities
		staticLog        *persist.Logger
		mu               sync.Mutex
		staticPersistDir string
		wg               sync.WaitGroup
		stopped          chan struct{}
	}
	// Stream is the interface for a Stream returned by the I3vMux. It's a
	// net.Conn with a few exceptions.
	Stream interface {
		// A stream implements net.Conn.
		net.Conn

		// SetPriority allows for prioritizing individual streams over others.
		SetPriority(int) error
	}
	// Handler is a method that gets called whenever a new stream is accepted.
	Handler func(stream Stream)

	muxInfo struct {
		addresses []string
		appSeed   appSeed
	}

	// blockedStream is a wrapper around the mux.Stream which blocks Read
	// operations until the underlying channel is closed. It is used in
	// NewStream to block the user from reading from the stream before the
	// subscription response is received.
	blockedStream struct {
		*mux.Stream
		c chan error
	}
)

// Read will block until c is closed. The first time Read is called after
// creating the stream it will return the result returned by c which is the
// result of the subscription response. If that err is != nil the underlying
// stream is already closed.
func (bs *blockedStream) Read(b []byte) (int, error) {
	if err := <-bs.c; err != nil {
		return 0, err
	}
	return bs.Stream.Read(b)
}

// New creates a new I3vMux which listens on 'address' for incoming connections.
// pubKey and privKey will be used to sign the message during the initial
// handshake for incoming connections.
func New(address string, log *persist.Logger, persistDir string) (*I3vMux, error) {
	// Listen on the specified address.
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return nil, err
	}
	// Create a client since it's the same as creating a server.
	sm := &I3vMux{
		staticAppSeed:    appSeed(fastrand.Uint64n(math.MaxUint64)),
		staticLog:        log,
		staticListener:   listener,
		handlers:         make(map[string]Handler),
		muxs:             make(map[appSeed]*mux.Mux),
		outgoingMuxs:     make(map[string]*mux.Mux),
		muxSet:           make(map[*mux.Mux]muxInfo),
		staticPersistDir: persistDir,
		stopped:          make(chan struct{}),
	}
	// Init the persistence.
	if err := sm.initPersist(); err != nil {
		return nil, errors.AddContext(err, "failed to initialize I3vMux persistence")
	}
	// Spawn the listening thread.
	sm.wg.Add(1)
	go func() {
		sm.threadedListen(listener)
		sm.wg.Done()
	}()
	return sm, nil
}

// Address returns the underlying listener's address.
func (sm *I3vMux) Address() net.Addr {
	return sm.staticListener.Addr()
}

// PublicKey returns the i3vmux's public key.
func (sm *I3vMux) PublicKey() mux.ED25519PublicKey {
	return sm.staticPubKey
}

// PrivateKey returns the i3vmux's private key.
func (sm *I3vMux) PrivateKey() mux.ED25519SecretKey {
	return sm.staticPrivKey
}

// Close closes the mux and waits for background threads to finish.
func (sm *I3vMux) Close() error {
	err := sm.staticListener.Close()
	close(sm.stopped)
	sm.wg.Wait()
	return err
}

// NewStream connects to an address if not yet connected and opens a new stream
// to the given subscriber.
func (sm *I3vMux) NewStream(subscriber, address string, expectedPubKey mux.ED25519PublicKey) (Stream, error) {
	// Check if the subscriber is an empty string. It should be valid to
	// subscribe to the empty string but to avoid developer errors we check for
	// it here on the "client" side.
	if subscriber == "" {
		return nil, errors.New("subscriber can't be empty string")
	}
	// Check if an outgoing mux exists already.
	sm.mu.Lock()
	m, exists := sm.outgoingMuxs[address]
	sm.mu.Unlock()
	var err error
	if !exists {
		m, err = sm.managedNewOutgoingMux(address, expectedPubKey)
	}
	if err != nil {
		return nil, err
	}
	// Create a new stream to subscribe.
	stream, err := m.NewStream()
	if err != nil {
		return nil, err
	}
	// Send the subscriber request to tell the other I3vMux what name we want to
	// subscribe to.
	if err := writeSubscriberRequest(stream, subscriber); err != nil {
		return nil, errors.Compose(err, stream.Close())
	}
	// Read the response from the other I3vMux.
	done := make(chan error, 1)
	go func() {
		defer close(done)
		subscribeErr, err := readSubscriberResponse(stream)
		if err != nil {
			done <- errors.Compose(err, stream.Close())
			return
		}
		if subscribeErr != nil {
			done <- errors.Compose(subscribeErr, stream.Close())
			return
		}
	}()
	return &blockedStream{
		Stream: stream,
		c:      done,
	}, nil
}

// addMux adds a mux to all of the I3vMuxs data structures.
func (sm *I3vMux) managedAddMux(mux *mux.Mux, appSeed appSeed, addresses []string) {
	sm.mu.Lock()
	defer sm.mu.Unlock()
	sm.muxSet[mux] = muxInfo{
		addresses: addresses,
		appSeed:   appSeed,
	}
	sm.muxs[appSeed] = mux
	for _, address := range addresses {
		sm.outgoingMuxs[address] = mux
	}
}

// managedMuxCallback is called whenever one of the I3vMux's muxs is closed for
// any reason. It will handle necessary cleanup for the I3vMux.
func (sm *I3vMux) managedMuxCallback(mux *mux.Mux) {
	sm.managedRemoveMux(mux)
}

// managedTimeoutCallback is called whenever one of the I3vMux's muxs is close
// to timing out. This should check if a mux is worth keeping alive and then
// send a manual keepalive signal.
func (sm *I3vMux) managedTimeoutCallback(mux *mux.Mux) {
	sm.mu.Lock()
	mi, exists := sm.muxSet[mux]
	sm.mu.Unlock()
	if !exists {
		return // mux was removed in meantime
	}
	if len(mi.addresses) == 0 {
		return // not outgoing mux
	}
	if err := mux.Keepalive(); err != nil {
		err = errors.Compose(err, mux.Close())
		sm.staticLog.Print("managedTimeoutCallback: failed to send keepalive", err)
		return
	}
}

// managedNewOutgoingMux creates a new mux which is connected to address.
func (sm *I3vMux) managedNewOutgoingMux(address string, expectedPubKey mux.ED25519PublicKey) (*mux.Mux, error) {
	// If not, establish a new connection and wrap it in a mux.
	conn, err := net.Dial("tcp", address)
	if err != nil {
		return nil, err
	}
	// Wrap the connection in a mux.
	m, err := mux.NewClientMux(conn, expectedPubKey, sm.staticLog, sm.managedMuxCallback, sm.managedTimeoutCallback)
	if err != nil {
		return nil, err
	}
	// Create a new stream to send the seed.
	stream, err := m.NewStream()
	if err != nil {
		return nil, err
	}
	// Send the seed request.
	if err := writeSeedRequest(stream, sm.staticAppSeed); err != nil {
		return nil, errors.Compose(err, stream.Close())
	}
	// Receive the seed response.
	peerSeed, err := readSeedResponse(stream)
	if err != nil {
		return nil, errors.Compose(err, stream.Close())
	}
	ephemeralSeed, err := deriveEphemeralAppSeed(sm.staticAppSeed, conn.RemoteAddr())
	if err != nil {
		return nil, errors.Compose(err, stream.Close())
	}
	appSeed := ephemeralSeed + peerSeed
	// Close the stream.
	if err := stream.Close(); err != nil {
		return nil, err
	}
	// Add the mux.
	sm.managedAddMux(m, appSeed, []string{address})
	// Start accepting streams on that mux.
	sm.wg.Add(1)
	go func() {
		sm.threadedAccept(m)
		sm.wg.Done()
	}()
	return m, nil
}

// managedRemoveMux removes a mux from all of the I3vMuxs data structures.
func (sm *I3vMux) managedRemoveMux(mux *mux.Mux) {
	sm.mu.Lock()
	defer sm.mu.Unlock()
	muxInfo, exists := sm.muxSet[mux]
	if !exists {
		return // mux wasn't added to the map yet.
	}
	delete(sm.muxSet, mux)
	delete(sm.muxs, muxInfo.appSeed)
	for _, address := range muxInfo.addresses {
		delete(sm.outgoingMuxs, address)
	}
}

// threadedListen starts listening for incoming TCP connections which are then
// upgraded using the mux.Mux.
func (sm *I3vMux) threadedListen(listener net.Listener) {
	// Start a goroutine that closes the listener when the mux is closed.
	sm.wg.Add(1)
	go func() {
		<-sm.stopped
		if err := listener.Close(); err != nil {
			sm.staticLog.Print("failed to close I3vMux listener:", err)
		}
		sm.wg.Done()
	}()
	for {
		// Accept new connection.
		conn, err := listener.Accept()
		if err != nil {
			// shutdown
			break
		}
		// Upgrade the connection using a mux.
		mux, err := mux.NewServerMux(conn, sm.staticPubKey, sm.staticPrivKey, sm.staticLog, sm.managedMuxCallback, sm.managedTimeoutCallback)
		if err != nil {
			sm.staticLog.Print("threadedListen: failed to create server mux", err)
			continue
		}
		// Get the first stream which contains the seed.
		seedStream, err := mux.AcceptStream()
		if err != nil {
			err = errors.Compose(err, mux.Close())
			sm.staticLog.Print("threadedListen: failed to accept seed stream", err)
			continue
		}
		peerSeed, err := readSeedRequest(seedStream)
		if err != nil {
			err = errors.Compose(seedStream.Close(), mux.Close())
			sm.staticLog.Print("threadedListen: failed to read seed request", err)
			continue
		}
		ephemeralSeed, err := deriveEphemeralAppSeed(sm.staticAppSeed, conn.RemoteAddr())
		if err != nil {
			err = errors.Compose(seedStream.Close(), mux.Close())
			sm.staticLog.Print("threadedListen: failed to derive shared seed", err)
			continue
		}
		appSeed := ephemeralSeed + peerSeed
		// Check if the seed exists already. If it does, we close the existing
		// mux and use the new one without error.
		sm.mu.Lock()
		m, exists := sm.muxs[appSeed]
		sm.mu.Unlock()
		if exists {
			err = m.Close()
			sm.managedRemoveMux(m)
			sm.staticLog.Printf("threadedListen: WARN: overwriting mux for seed %v, %v", appSeed, err)
		}
		// Add the mux.
		sm.managedAddMux(mux, appSeed, nil)
		// Write response.
		err = writeSeedResponse(seedStream, sm.staticAppSeed)
		if err != nil {
			err = errors.Compose(seedStream.Close(), mux.Close())
			sm.staticLog.Print("threadedListen: failed to write seed response", err)
			continue
		}

		// Spawn a thread to start listening on the mux.
		sm.wg.Add(1)
		go func() {
			sm.threadedAccept(mux)
			sm.managedRemoveMux(mux)
			sm.wg.Done()
		}()
	}
}

// deriveEphemeralAppSeed derives an ephemeral appSeed for a peer.
func deriveEphemeralAppSeed(seed appSeed, address net.Addr) (appSeed, error) {
	host, _, err := net.SplitHostPort(address.String())
	if err != nil {
		return 0, err
	}
	rawSeed := crypto.HashAll(seed, host)
	return appSeed(binary.LittleEndian.Uint64(rawSeed[:])), nil
}
