package helpers

import (
	"os"
	"path/filepath"
	"time"
)

var (
	// I3vTestingDir is the directory that contains all of the files and
	// folders created during testing.
	I3vTestingDir = filepath.Join(os.TempDir(), "I3vTesting")
)

// Retry will call 'fn' 'tries' times, waiting 'durationBetweenAttempts'
// between each attempt, returning 'nil' the first time that 'fn' returns nil.
// If 'nil' is never returned, then the final error returned by 'fn' is
// returned.
func Retry(tries int, durationBetweenAttempts time.Duration, fn func() error) (err error) {
	for i := 1; i < tries; i++ {
		err = fn()
		if err == nil {
			return nil
		}
		time.Sleep(durationBetweenAttempts)
	}
	return fn()
}

// TempDir joins the provided directories and prefixes them with the I3v
// testing directory.
func TempDir(dirs ...string) string {
	path := filepath.Join(I3vTestingDir, filepath.Join(dirs...))
	os.RemoveAll(path) // remove old test data
	return path
}
