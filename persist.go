package i3vmux

import (
	"os"
	"path/filepath"

	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/i3vmux/mux"
)

const (
	// persistDirPerms are the permissions used when creating the persist dir of
	// the I3vMux.
	persistDirPerms = 0700
	// settingsName is the name of the file which stores the i3vmux settings
	// a.k.a. the persistence struct.
	settingsName = "i3vmux.json"
)

var (
	// persistMetadata is the metadata written to disk when persistig the
	// I3vMux.
	persistMetadata = persist.Metadata{
		Header:  "I3vMux",
		Version: "1.4.2.1",
	}
)

type (
	// persistence is the data persisted to disk by the I3vMux.
	persistence struct {
		PubKey  mux.ED25519PublicKey `json:"pubkey"`
		PrivKey mux.ED25519SecretKey `json:"privkey"`
	}
)

// initPersist loads the persistene of the I3vMux or creates a new one with
// fresh keys in case it doesn't exist yet.
func (sm *I3vMux) initPersist() error {
	// Create the persist dir.
	if err := os.MkdirAll(sm.staticPersistDir, persistDirPerms); err != nil {
		return errors.AddContext(err, "failed to create I3vMux persist dir")
	}
	// Get the filepath.
	path := filepath.Join(sm.staticPersistDir, settingsName)
	// Load the persistence object
	var data persistence
	err := persist.LoadJSON(persistMetadata, &data, path)
	if os.IsNotExist(err) {
		// If the data isn't persisted yet we create new keys and persist them.
		privKey, pubKey := mux.GenerateED25519KeyPair()
		data.PrivKey = privKey
		data.PubKey = pubKey
		if err := persist.SaveJSON(persistMetadata, data, path); err != nil {
			return errors.AddContext(err, "failed to initialize fresh persistence")
		}
	}
	// Set the fields in the I3vMux
	sm.staticPrivKey = data.PrivKey
	sm.staticPubKey = data.PubKey
	return nil
}

// persistData creates a persistence object from the I3vMux.
func (sm *I3vMux) persistData() persistence {
	return persistence{
		PubKey:  sm.staticPubKey,
		PrivKey: sm.staticPrivKey,
	}
}

// savePersist writes the persisted fields of the I3vMux to disk.
func (sm *I3vMux) savePersist() error {
	path := filepath.Join(sm.staticPersistDir, settingsName)
	return errors.AddContext(persist.SaveJSON(persistMetadata, sm.persistData(), path), "failed to save persist data")
}
