package i3vmux

import (
	"math"
	"net"

	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/fastrand"
	"gitlab.com/I3VNetDisk/i3vmux/mux"
)

// CompatV1421NewWithKeyPair is like New but will use the provided keys instead
// of generated or existing ones. For safety this should only be called the
// first time after upgrading a node since it will always use the provided keys
// instead of the existing ones.
func CompatV1421NewWithKeyPair(address string, log *persist.Logger, persistDir string, privKey mux.ED25519SecretKey, pubKey mux.ED25519PublicKey) (*I3vMux, error) {
	// Listen on the specified address.
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return nil, err
	}
	// Create a client since it's the same as creating a server.
	sm := &I3vMux{
		staticAppSeed:    appSeed(fastrand.Uint64n(math.MaxUint64)),
		staticLog:        log,
		staticListener:   listener,
		handlers:         make(map[string]Handler),
		muxs:             make(map[appSeed]*mux.Mux),
		outgoingMuxs:     make(map[string]*mux.Mux),
		muxSet:           make(map[*mux.Mux]muxInfo),
		staticPersistDir: persistDir,
		stopped:          make(chan struct{}),
	}
	// Init the persistence.
	if err := sm.initPersist(); err != nil {
		return nil, errors.AddContext(err, "failed to initialize I3vMux persistence")
	}
	// Overwrite the keys.
	sm.staticPubKey = pubKey
	sm.staticPrivKey = privKey
	if err := sm.savePersist(); err != nil {
		return nil, errors.AddContext(err, "failed to override keys")
	}
	// Spawn the listening thread.
	sm.wg.Add(1)
	go func() {
		sm.threadedListen(listener)
		sm.wg.Done()
	}()
	return sm, nil
}
