package mux

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"time"

	"gitlab.com/I3VNetDisk/I3v/encoding"
	"gitlab.com/I3VNetDisk/errors"
)

var (
	errSmallPacketSize         = errors.New("packetSize < minPacketSize")
	errSmallFrameSize          = errors.New("frameSize < lowerFrameSize")
	errBigFrameSize            = errors.New("frameSize > upperFrameSize")
	errSmallMaxTimeout         = errors.New("maxTimeout < lowerMaxTimeout")
	errSmallStreamTimeout      = errors.New("maxStreamTimeout < lowerStreamTimeout")
	errStreamTimeoutSmallerMax = errors.New("maxTimeout < maxStreamTimeout")
)

// isConnClosed returns true if the error occurred due to reading from a closed
// connection.
func isConnClosed(err error) bool {
	return errors.Contains(err, io.EOF) ||
		(err != nil && strings.Contains(err.Error(), "use of closed network connection"))
}

// mergeConnSettings takes 2 sets of settings and merges them into one. The
// resulting connections are the agreed upon ones between client and server.
func mergeConnSettings(settings1, settings2 connectionSettings) (connectionSettings, error) {
	// Agree upon the smaller packet size as long as it's not below the minimum.
	packetSize := settings1.RequestedPacketSize
	if settings2.RequestedPacketSize < settings1.RequestedPacketSize {
		packetSize = settings2.RequestedPacketSize
	}
	if packetSize < minPacketSize {
		return connectionSettings{}, errors.Compose(errSmallPacketSize, fmt.Errorf("%v < %v", packetSize, minPacketSize))
	}
	// Agree upon the lower frameSize which should be between 10 and 64 packets.
	frameSize := settings1.FrameSize
	if settings2.FrameSize < settings1.FrameSize {
		frameSize = settings2.FrameSize
	}
	if frameSize < lowerFrameSize {
		return connectionSettings{}, errors.Compose(errSmallFrameSize, fmt.Errorf("%v < %v", frameSize, lowerFrameSize))
	}
	if frameSize > upperFrameSize {
		return connectionSettings{}, errors.Compose(errBigFrameSize, fmt.Errorf("%v < %v", frameSize, upperFrameSize))
	}
	// Agree upon the lower MaxTimeout.
	maxTimeout := settings1.MaxTimeout
	if settings2.MaxTimeout < settings1.MaxTimeout {
		maxTimeout = settings2.MaxTimeout
	}
	if maxTimeout < LowerMaxTimeout {
		return connectionSettings{}, errors.Compose(errSmallMaxTimeout, fmt.Errorf("%v < %v", maxTimeout, LowerMaxTimeout))
	}
	return connectionSettings{
		RequestedPacketSize: packetSize,
		FrameSize:           frameSize,
		MaxTimeout:          maxTimeout,
	}, nil
}

// initClient does the initialization for a new client mux. This consists of
// establishing encryption and sending the first updateSettingsFrame.
func (m *Mux) initClient(expectedPubKey ED25519PublicKey) error {
	if err := m.initEncryptionClient(expectedPubKey); err != nil {
		return err
	}
	return m.initUpdateSettingsClient()
}

// initServer does the initialization for a new server mux. This consists of
// establishing encryption and receiving the first updateSettingsFrame from the
// client.
func (m *Mux) initServer() error {
	if err := m.initEncryptionServer(); err != nil {
		return err
	}
	return m.initUpdateSettingsServer()
}

// initEncryptionClient sends an encryption request frame with an ephemeral key
// to the server and performs the key exchange necessary to upgrade the
// connection to an encrypted connection.
func (m *Mux) initEncryptionClient(expectedPubKey ED25519PublicKey) error {
	// Create the request for the handshake.
	requestSecretKey, requestPublicKey, eerf := newEstablishEncryptionRequestFrame()
	// Marshal the frame.
	f, err := eerf.Marshal()
	if err != nil {
		return errors.AddContext(err, "failed to marshal encryptionRequest frame")
	}
	// Send the frame.
	_, err = m.staticConn.Write(f)
	if err != nil {
		return errors.AddContext(err, "failed to send encryptionRequest frame")
	}
	// Receive the response.
	resp := make([]byte, marshaledEstablishEncryptionResponseFrameSize)
	_, err = io.ReadFull(m.staticConn, resp)
	if err != nil {
		return errors.AddContext(err, "failed to read encryptionResponse frame")
	}
	// Unmarshal the response frame.
	var respFrame frame
	if err := respFrame.Unmarshal(resp); err != nil {
		return errors.AddContext(err, "failed to unmarshal response frame")
	}
	// Check that the correct flag was set.
	if respFrame.id != frameIDEstablishEncryption {
		return fmt.Errorf("expected response frame to have flag %v but was %v", frameIDEstablishEncryption, respFrame.id)
	}
	// Unmarshal the establishEncryptionResponse.
	var eer establishEncryptionResponse
	if err := encoding.Unmarshal(respFrame.payload, &eer); err != nil {
		return errors.AddContext(err, "failed to unmarshal establishEncryptionResponse")
	}
	// verify the signature
	// TODO: If this fails, the host either constructed the wrong hash or signed
	// the hash with the wrong key. No way to tell exactly. All we know is that
	// the host is not authenticated which might be good enough. We can still
	// upgrade the connection to be encrypted.
	hash := createSignatureHash(eer.PublicKey, requestPublicKey)
	if !verifyHash(hash, expectedPubKey, eer.Signature) {
		return errors.New("invalid signature")
	}
	// Derive the shared key and prepare the cipher.
	sharedSecret := deriveSharedSecret(requestSecretKey, X25519PublicKey(eer.PublicKey))
	m.staticAead, err = initCipher(sharedSecret[:], eer.Cipher)
	if err != nil {
		return errors.AddContext(err, "failed to init cipher")
	}
	return nil
}

// initEncryptionServer waits for the encryption request from the client to
// perform the key exchange required to upgrade the connection to an encryption
// connection.
func (m *Mux) initEncryptionServer() error {
	// Receive the establishEncryptionRequestFrame.
	reqFrameData := make([]byte, marshaledEstablishEncryptionRequestFrameSize)
	if _, err := io.ReadFull(m.staticConn, reqFrameData); err != nil {
		return errors.AddContext(err, "failed to read establishEncryptionRequestFrame")
	}
	// Unmarshal the request frame.
	var reqFrame frame
	if err := reqFrame.Unmarshal(reqFrameData); err != nil {
		return errors.AddContext(err, "failed to unmarshale establishEncryptionRequestFrame")
	}
	// Check that the correct flag was set.
	if reqFrame.id != frameIDEstablishEncryption {
		return fmt.Errorf("expected request frame to have flag %v but was %v", frameIDEstablishEncryption, reqFrame.id)
	}
	// Unmarshal the establishEncryptionRequest.
	var eer establishEncryptionRequest
	if err := encoding.Unmarshal(reqFrame.payload, &eer); err != nil {
		return errors.AddContext(err, "failed to unmarshal establishEncryptionRequest")
	}
	// Prepare the response.
	xsk, cipher, respFrame := newEstablishEncryptionResponseFrame(eer.PublicKey, m.privKey)
	// Marshal the response.
	respFrameData, err := respFrame.Marshal()
	if err != nil {
		return errors.AddContext(err, "failed to marshal response frame")
	}
	// Send response.
	if _, err := m.staticConn.Write(respFrameData); err != nil {
		return errors.AddContext(err, "failed to send response frame")
	}
	// Derive the shared key
	sharedSecret := deriveSharedSecret(xsk, eer.PublicKey)
	m.staticAead, err = initCipher(sharedSecret[:], cipher)
	if err != nil {
		return errors.AddContext(err, "failed to init cipher")
	}
	return nil
}

// initUpdateSettingsClient sends the first updateSettingsFrame to the server to
// initialize the connection.
func (m *Mux) initUpdateSettingsClient() error {
	// Create the updateSettingsFrame and send it to the server.
	clientSettings := newUpdateConnectionSettingsFrame(m.settings)
	if _, err := m.managedWriteFrame(clientSettings); err != nil {
		return errors.AddContext(err, "client failed to write clientSettings")
	}
	// Read the server settings.
	serverSettings, err := m.managedReadFrame()
	if err != nil {
		return errors.AddContext(err, "client failed to read serverSettings frame")
	}
	if serverSettings.id != frameIDUpdateSettings {
		return fmt.Errorf("expected frameID to be %v but was %v", frameIDUpdateSettings, serverSettings.id)
	}
	var settings connectionSettings
	if err := encoding.Unmarshal(serverSettings.payload, &settings); err != nil {
		return errors.AddContext(err, "failed to unmarshal connection settings")
	}
	// Merge client and server settings.
	m.settings, err = mergeConnSettings(m.settings, settings)
	return err
}

// initUpdateSettingsServer receives the first updateSettingsFrame from the
// client to initialize the connection.
func (m *Mux) initUpdateSettingsServer() error {
	// Read the settings frame.
	clientSettings, err := m.managedReadFrame()
	if err != nil {
		return errors.AddContext(err, "failed to read clientSettings frame")
	}
	if clientSettings.id != frameIDUpdateSettings {
		return fmt.Errorf("expected frameID to be %v but was %v", frameIDUpdateSettings, clientSettings.id)
	}
	var settings connectionSettings
	if err := encoding.Unmarshal(clientSettings.payload, &settings); err != nil {
		return errors.AddContext(err, "failed to unmarshal connection settings")
	}
	// Respond with own settings.
	serverSettings := newUpdateConnectionSettingsFrame(m.settings)
	if _, err := m.managedWriteFrame(serverSettings); err != nil {
		return errors.AddContext(err, "client failed to write serverSettings")
	}
	// Merge client and server settings.
	m.settings, err = mergeConnSettings(m.settings, settings)
	return err
}

// managedAcceptStream listens for an incoming stream and returns it. If the mux
// is stopped while waiting for a new stream, 'nil' is returned.
func (m *Mux) managedAcceptStream() (s *Stream, err error) {
	// Wait for a stream to be available.,
	select {
	case <-m.stopped:
		return nil, io.EOF
	case <-m.newStreamsChan:
	}

	// Grab it.
	m.mu.Lock()
	defer m.mu.Unlock()
	if len(m.newStreams) == 0 {
		err := errors.New("m.newStreams shouldn't have len 0")
		os.Stderr.WriteString(err.Error())
		return nil, err
	}
	s, m.newStreams = m.newStreams[0], m.newStreams[1:]
	return s, nil
}

// managedReadFrame updates the deadline on the connection and reads a frame
// from the mux's underlying connection.
func (m *Mux) managedReadFrame() (frame, error) {
	m.mu.Lock()
	// update the connection's deadline for every read.
	err := m.updateDeadline()
	if err != nil {
		return frame{}, err
	}
	// read the frame.
	conn, aead, frameSize := m.staticConn, m.staticAead, int(m.settings.FrameSize)
	m.mu.Unlock()
	return readFrame(conn, aead, frameSize)
}

// managedWriteFrame updates the deadline on the connection and writes a frame
// to the mux's underlying connection.
func (m *Mux) managedWriteFrame(f frame) (int, error) {
	m.mu.Lock()
	// update the connection's deadline for every read.
	err := m.updateDeadline()
	if err != nil {
		return 0, err
	}
	conn, aead, frameSize := m.staticConn, m.staticAead, int(m.settings.FrameSize)
	m.mu.Unlock()
	return writeFrame(conn, aead, f, frameSize)
}

// managedWrite splits up the provided data into frames with the specified id
// and writes them to the underlying connection, blocking until all frames are
// written.
func (m *Mux) managedWrite(b []byte, s *Stream, timeout <-chan time.Time) (int, error) {
	id := s.staticID
	// Split the data up into frames.
	m.mu.Lock()
	maxPayload := maxFrameSize(int(m.settings.FrameSize), m.staticAead) - marshaledFrameHeaderSize
	m.mu.Unlock()

	// Force a timeout using the dependency.
	m.staticDeps.Disrupt("delayWrite")

	// Start writing the data frame by frame.
	buf := bytes.NewBuffer(b)
	written := 0
	for payload := buf.Next(maxPayload); len(payload) > 0; payload = buf.Next(maxPayload) {
		// Prepare payload frame.
		f := newPayloadFrame(id, payload)
		// Create a timer for the stream's deadline.
		done := make(chan struct{})
		var err error
		go func() {
			defer close(done)
			// Write the frame.
			_, err = m.managedWriteFrame(f)
		}()
		// Check for timeout.
		select {
		case <-timeout:
			// Write timed out. Return early.
			return written, ErrStreamTimedOut
		case <-done:
		}
		// Write is done. Increment 'written'.
		written += len(payload)
		// Check result.
		if err != nil {
			return written, err
		}
	}
	return len(b), nil
}

// managedRemoveStream closes a stream and removes it from the Mux.
func (m *Mux) managedRemoveStream(frameID uint32, err error) (bool, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	// Search established streams.
	stream, exists := m.streams[frameID]
	if exists {
		delete(m.streams, frameID)
		return true, stream.managedClose(err)
	}
	// Search new streams.
	for i, stream := range m.newStreams {
		if stream.staticID == frameID {
			m.newStreams = append(m.newStreams[:i], m.newStreams[i+1:]...)
			// remove element from channel too since we got now space for 1 more
			// stream in newStreams.
			<-m.newStreamsChan
			return true, stream.managedClose(err)
		}
	}
	return false, nil
}

// managedWriteErrorFrame is a convenience method to create an error frame from
// a given id and error and write it to the underlying connection of the mux.
func (m *Mux) managedWriteErrorFrame(id uint32, err error) error {
	ef := newErrorFrame(id, err)
	_, err = m.managedWriteFrame(ef)
	return err
}

// managedWriteFinalFrame is a convenience method to create a frame from a given
// id with the finalFrame flag set and write it to the underlying connection of
// the mux.
func (m *Mux) managedWriteFinalFrame(id uint32) error {
	ef := newFinalFrame(id)
	_, err := m.managedWriteFrame(ef)
	return err
}

// managedWriteKeepaliveFrame writes a keepalive frame with the corresponding id
// to the underlying connection of the mux.
func (m *Mux) managedWriteKeepaliveFrame() error {
	kf := newKeepaliveFrame()
	_, err := m.managedWriteFrame(kf)
	return err
}

// threadedReceiveData is a background thread which keeps fetching data from the
// mux's underlying connection.
func (m *Mux) threadedReceiveData() {
	for {
		select {
		case <-m.stopped:
			return // mux was stopped
		default:
		}
		frame, err := m.managedReadFrame()
		if err, ok := err.(net.Error); ok && err.Timeout() {
			closeErr := errors.Compose(err, m.managedClose())
			m.log.Print("connection timed out:", closeErr)
			return // connection timed out
		}
		if isConnClosed(err) {
			err = errors.Compose(err, m.managedClose())
			m.log.Print("readFrame failed due to connection error:", err)
			return // can't recover from connection error
		}
		if err != nil {
			m.log.Print("readFrame failed:", err)
			continue
		}
		// Check for special flags.
		finalFrame := frame.flags&frameBitFinalFrame > 0
		errorFrame := frame.flags&frameBitErrorFrame > 0

		// Check if errorFrame bit was set correctly and handle it.
		if errorFrame {
			if !finalFrame {
				err = errors.New("can't set errorFrame bit without finalFrame bit")
			}
			if err != nil {
				m.log.Print(err)
			}
		}
		// Check if it is necessary to close the stream.
		if finalFrame || errorFrame {
			// Return io.EOF for a finalFrame and the payload of the error frame
			// for an errorFrame.
			respErr := io.EOF
			if errorFrame {
				respErr = errors.New(string(frame.payload))
			}
			removed, err := m.managedRemoveStream(frame.id, respErr) // either error from above if flags were inconsistent or io.EOF on 'nil'
			if err != nil {
				m.log.Print("failed to remove stream:", err)
			}
			if !removed {
				m.log.Print("stream to remove wasn't found:", frame.id)
			}
			m.log.Print("stream removed successfully:", finalFrame, errorFrame, frame.id)
			continue
		}
		// Check for special ids.
		switch frame.id {
		case frameIDUpdateSettings:
			// TODO (followup): update settings. can this be done by both peers?
			continue
		case frameIDKeepalive:
			continue
		default:
		}
		// Send back an error with the same frame id set.
		if frame.id == frameIDErrorBadInit || frame.id < numReservedFrameIDs {
			err := m.managedWriteErrorFrame(frame.id, errors.New("unknown frame id"))
			if err != nil {
				m.log.Print("failed to write error frame:", err)
			}
			continue
		}
		// Handle regular frame. If the id exists already, write the data to the
		// streams buffer. If not, create a new stream.
		m.mu.Lock()
		stream, exists := m.streams[frame.id]
		if !exists {
			stream = m.newStream(frame.id)
			// add stream to newStreams slice.
			m.newStreams = append(m.newStreams, stream)
			// signal that a new stream was added.
			m.mu.Unlock()
			select {
			case <-m.stopped:
				return // mux stopped
			case m.newStreamsChan <- struct{}{}:
			}
		} else {
			m.mu.Unlock()
		}
		// Write payload to stream.
		if _, err := stream.staticW.Write(frame.payload); err != nil {
			m.log.Print("failed to write frame to corresponding stream", err)
			continue
		}
	}
}
