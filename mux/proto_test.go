package mux

import (
	"reflect"
	"testing"

	"gitlab.com/I3VNetDisk/errors"
)

// TestMergeConnSettings tests if mergeConnSettings works as expected including
// all edge cases.
func TestMergeConnSettings(t *testing.T) {
	// connection settings with very low values
	lowerSettings := connectionSettings{
		RequestedPacketSize: minPacketSize,
		FrameSize:           lowerFrameSize,
		MaxTimeout:          LowerMaxTimeout,
	}
	// connection settings with higher values
	upperSettings := connectionSettings{
		RequestedPacketSize: minPacketSize + 1,
		FrameSize:           upperFrameSize,
		MaxTimeout:          LowerMaxTimeout + 1,
	}
	// Merge the settings.
	merged1, err1 := mergeConnSettings(lowerSettings, upperSettings)
	merged2, err2 := mergeConnSettings(upperSettings, lowerSettings)
	if err := errors.Compose(err1, err2); err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(merged1, merged2) {
		t.Log(merged1)
		t.Log(merged2)
		t.Fatal("merged1 and merged2 are not equal")
	}
	// The merged settings should equal the lowerSettings.
	if !reflect.DeepEqual(merged1, lowerSettings) {
		t.Log(merged1)
		t.Log(lowerSettings)
		t.Fatal("merged1 and lowerSettings are not equal")
	}
	// Test packetSize < minPacketSize
	errSettings := lowerSettings
	errSettings.RequestedPacketSize--
	_, err := mergeConnSettings(errSettings, lowerSettings)
	if !errors.Contains(err, errSmallPacketSize) {
		t.Fatal(err)
	}
	// Test frameSize < lowerFrameSize
	errSettings = lowerSettings
	errSettings.FrameSize--
	_, err = mergeConnSettings(errSettings, lowerSettings)
	if !errors.Contains(err, errSmallFrameSize) {
		t.Fatal(err)
	}
	// Test frameSize > upperFrameSize
	errSettings = upperSettings
	errSettings.FrameSize++
	_, err = mergeConnSettings(errSettings, errSettings)
	if !errors.Contains(err, errBigFrameSize) {
		t.Fatal(err)
	}
	// Test maxTimeout < lowerMaxTimeout
	errSettings = lowerSettings
	errSettings.MaxTimeout--
	_, err = mergeConnSettings(errSettings, lowerSettings)
	if !errors.Contains(err, errSmallMaxTimeout) {
		t.Fatal(err)
	}
}
