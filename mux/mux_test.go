package mux

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"net"
	"runtime"
	"sync"
	"testing"
	"time"

	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/fastrand"
	"gitlab.com/I3VNetDisk/i3vmux/helpers"
)

// createTestingMuxs creates a connected pair of type Mux which has already
// completed the encryption handshake and is ready to go.
func createTestingMuxs() (clientMux, serverMux *Mux) {
	// Prepare tcp connections.
	clientConn, serverConn := createTestingConns()
	// Generate server keypair.
	serverPrivKey, serverPubKey := GenerateED25519KeyPair()

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		var err error
		clientMux, err = NewClientMux(clientConn, serverPubKey, persist.NewLogger(ioutil.Discard), func(*Mux) {}, func(*Mux) {})
		if err != nil {
			panic(err)
		}
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
		var err error
		serverMux, err = NewServerMux(serverConn, serverPubKey, serverPrivKey, persist.NewLogger(ioutil.Discard), func(*Mux) {}, func(*Mux) {})
		if err != nil {
			panic(err)
		}
	}()
	wg.Wait()
	return
}

// createTestingConns is a helper method to create a pair of connected tcp
// connection ready to use.
func createTestingConns() (clientConn, serverConn net.Conn) {
	ln, _ := net.Listen("tcp", "127.0.0.1:0")
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		serverConn, _ = ln.Accept()
		wg.Done()
	}()
	clientConn, _ = net.Dial("tcp", ln.Addr().String())
	wg.Wait()
	return
}

// TestCreateTestingCons tests the createTestingConns helper to make sure the
// connections are open and ready to use.
func TestCreateTestingConns(t *testing.T) {
	// Create some data the client sends the server and vice-versa.
	data := fastrand.Bytes(100)
	clientConn, serverConn := createTestingConns()
	var wg sync.WaitGroup

	// Client
	wg.Add(1)
	go func() {
		defer wg.Done()
		_, err := clientConn.Write(data)
		if err != nil {
			t.Fatal(err)
		}
		d := make([]byte, len(data))
		if _, err := io.ReadFull(clientConn, d); err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(d, data) {
			t.Fatal("received data doesn't match data")
		}
	}()
	// Server
	wg.Add(1)
	go func() {
		defer wg.Done()
		d := make([]byte, len(data))
		if _, err := io.ReadFull(serverConn, d); err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(d, data) {
			t.Fatal("received data doesn't match data")
		}
		_, err := serverConn.Write(data)
		if err != nil {
			t.Fatal(err)
		}
	}()
	// Wait for client and server to be done.
	wg.Wait()
	if err := clientConn.Close(); err != nil {
		t.Fatal(err)
	}
	if err := serverConn.Close(); err != nil {
		t.Fatal(err)
	}
}

// TestCreateTestingMuxs tests if createTestingMuxs produces a couple of
// connected multiplexers.
func TestCreateTestingMuxs(t *testing.T) {
	client, server := createTestingMuxs()
	defer client.Close()
	defer server.Close()
	data := fastrand.Bytes(int(client.settings.FrameSize) * 5)

	var wg sync.WaitGroup
	// Server thread.
	wg.Add(1)
	go func() {
		defer wg.Done()
		// Wait for a stream.
		stream, err := server.AcceptStream()
		if err != nil {
			t.Fatal(err)
		}
		// Read some data.
		receivedData := make([]byte, len(data))
		if _, err := io.ReadFull(stream, receivedData); err != nil {
			t.Fatal(err)
		}
		// The data should match.
		if !bytes.Equal(receivedData, data) {
			t.Fatal("server: received data didn't match")
		}
		// Send the data back.
		written, err := stream.Write(receivedData)
		if err != nil {
			t.Fatal(err)
		}
		if written < len(receivedData) {
			t.Fatalf("server: not enough data written: %v < %v", written, len(receivedData))
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}()
	// Client thread.
	wg.Add(1)
	go func() {
		defer wg.Done()
		// Create a new stream.
		stream, err := client.NewStream()
		if err != nil {
			t.Fatal(err)
		}
		// Write some data.
		written, err := stream.Write(data)
		if err != nil {
			t.Fatal(err)
		}
		if written != len(data) {
			t.Fatalf("client: not enough data written: %v < %v", written, len(data))
		}
		// Read some data.
		receivedData := make([]byte, len(data))
		if _, err := io.ReadFull(stream, receivedData); err != nil {
			t.Fatal(err)
		}
		// The data should match.
		if !bytes.Equal(receivedData, data) {
			t.Fatal("client: received data didn't match")
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}()
	// Wait for client and server to be done.
	wg.Wait()
}

// TestMuxSendReceiveParallel starts a large number of threads in parallel which
// open streams and send data back and forth.
func TestMuxSendReceiveParallel(t *testing.T) {
	client, server := createTestingMuxs()
	defer client.Close()
	defer server.Close()
	data := fastrand.Bytes(int(client.settings.FrameSize) * 5)

	start := make(chan struct{})
	// Server thread.
	serverWorker := func() {
		<-start
		// Wait for a stream.
		stream, err := server.AcceptStream()
		if err != nil {
			t.Error(err)
			return
		}
		// Read some data.
		receivedData := make([]byte, len(data))
		if _, err := io.ReadFull(stream, receivedData); err != nil {
			t.Error(err)
			return
		}
		// The data should match.
		if !bytes.Equal(receivedData, data) {
			t.Error("server: received data didn't match")
			return
		}
		// Send the data back.
		written, err := stream.Write(receivedData)
		if err != nil {
			t.Error(err)
			return
		}
		if written < len(receivedData) {
			t.Errorf("server: not enough data written: %v < %v", written, len(receivedData))
			return
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Error(err)
			return
		}
	}
	// Client thread.
	clientWorker := func() {
		<-start
		// Create a new stream.
		stream, err := client.NewStream()
		if err != nil {
			t.Error(err)
			return
		}
		// Write some data.
		written, err := stream.Write(data)
		if err != nil {
			t.Error(err)
			return
		}
		if written < len(data) {
			t.Errorf("client: not enough data written: %v < %v", written, len(data))
			return
		}
		// Read some data.
		receivedData := make([]byte, len(data))
		if _, err := io.ReadFull(stream, receivedData); err != nil {
			t.Error(err)
			return
		}
		// The data should match.
		if !bytes.Equal(receivedData, data) {
			t.Error("client: received data didn't match")
			return
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Error(err)
			return
		}
	}
	// Spin up the thread pairs.
	var wg sync.WaitGroup
	numThreadPairs := runtime.NumCPU() * 10
	for i := 0; i < numThreadPairs; i++ {
		wg.Add(2)
		go func() {
			defer wg.Done()
			clientWorker()
		}()
		go func() {
			defer wg.Done()
			serverWorker()
		}()
	}
	// Wait for client and server threads to be done.
	close(start)
	wg.Wait()
}

// TestErrorFrame makes sure that reading from a stream that was closed with an
// error frame by the peer will return the correct error.
func TestErrorFrame(t *testing.T) {
	client, server := createTestingMuxs()
	defer client.Close()
	defer server.Close()
	data := fastrand.Bytes(int(client.settings.FrameSize) * 5)
	expectedErr := errors.New("TestErrorFrame")

	start := make(chan struct{})
	// Server thread.
	serverWorker := func() {
		<-start
		// Wait for a stream.
		stream, err := server.AcceptStream()
		if err != nil {
			t.Fatal(err)
		}
		// Keep reading data. At some point we should get an error since the
		// client sent an error frame.
		receivedData := make([]byte, len(data))
		var readErr error
		err = helpers.Retry(100, 100*time.Millisecond, func() error {
			_, readErr = stream.Read(receivedData)
			if readErr == nil {
				return errors.New("read was successful")
			}
			return nil
		})
		if err != nil {
			t.Fatal(readErr)
		}
		// The error we read from the stream should be the error from the error
		// frame.
		if readErr.Error() != expectedErr.Error() {
			t.Fatalf("expected error to be %v but was %v", expectedErr, readErr)
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Client thread.
	clientWorker := func() {
		<-start
		// Create a new stream.
		stream, err := client.NewStream()
		if err != nil {
			t.Fatal(err)
		}
		// Write half the data.
		_, err = stream.Write(data[:len(data)/2])
		if err != nil {
			t.Fatal(err)
		}
		// Write an error frame.
		err = client.managedWriteErrorFrame(stream.staticID, errors.New("TestErrorFrame"))
		if err != nil {
			t.Fatal(err)
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Spin up the threads.
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		clientWorker()
	}()
	go func() {
		defer wg.Done()
		serverWorker()
	}()
	// Wait for client and server threads to be done.
	close(start)
	wg.Wait()
}

// TestCloseStream tests that a stream that gets closed correctly causes the
// other end of the pipe to get closed with io.EOF as well instead of blocking
// on Read.
func TestCloseStream(t *testing.T) {
	client, server := createTestingMuxs()
	defer client.Close()
	defer server.Close()
	data := fastrand.Bytes(int(client.settings.FrameSize) * 5)

	start := make(chan struct{})
	// Server thread.
	serverWorker := func() {
		<-start
		// Wait for a stream.
		stream, err := server.AcceptStream()
		if err != nil {
			t.Fatal(err)
		}
		// Keep reading data. At some point we should get an error since the
		// client sent an error frame.
		receivedData := make([]byte, len(data))
		var readErr error
		err = helpers.Retry(100, 100*time.Millisecond, func() error {
			_, readErr = stream.Read(receivedData)
			if readErr == nil {
				return errors.New("read was successful")
			}
			return nil
		})
		if err != nil {
			t.Fatal(readErr)
		}
		// The error we read from the stream should be the error from the error
		// frame.
		if readErr.Error() != io.EOF.Error() {
			t.Fatalf("expected error to be %v but was %v", io.EOF, readErr)
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Client thread.
	clientWorker := func() {
		<-start
		// Create a new stream.
		stream, err := client.NewStream()
		if err != nil {
			t.Fatal(err)
		}
		// Write half the data.
		_, err = stream.Write(data[:len(data)/2])
		if err != nil {
			t.Fatal(err)
		}
		// Close the stream.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Spin up the threads.
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		clientWorker()
	}()
	go func() {
		defer wg.Done()
		serverWorker()
	}()
	// Wait for client and server threads to be done.
	close(start)
	wg.Wait()
}
