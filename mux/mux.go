package mux

import (
	"crypto/cipher"
	"encoding/binary"
	"net"
	"sync"
	"time"

	"gitlab.com/I3VNetDisk/I3v/persist"
	"gitlab.com/I3VNetDisk/errors"
	"gitlab.com/I3VNetDisk/fastrand"
	"gitlab.com/I3VNetDisk/i3vmux/deps"
)

var (
	// minPacketSize is the minimum allowed packet size which is derived from
	// the IPv6 required link MTU of 1280. 40 bytes of that will be lost to
	// IPv6, and 20 more will be lost to TCP.
	minPacketSize = uint16(1220)

	// lowerFrameSize is the minimum frameSize that two peers can agree upon.
	lowerFrameSize = 10 * uint32(minPacketSize)

	// upperFrameSize is the maximum frameSize that two peers can agree upon.
	upperFrameSize = 64 * uint32(minPacketSize)

	// numReservedFrameIDs describes the number of reserved IDs for frames. In
	// other words it's the first frame ID which can be assigned to new streams.
	numReservedFrameIDs = uint32(256)

	// LowerMaxTimeout is the lower bound for the maxTimeout that two peers can
	// agree upon.
	LowerMaxTimeout = uint16(120)
)

var (
	// DefaultMaxTimeout is the default value used for the mux's maxTimeout.
	DefaultMaxTimeout = uint16(1200) // 20 minutes
	// DefaultMaxStreamTimeout is the default value used for an established
	// stream.
	DefaultMaxStreamTimeout = uint16(600) // 10 minutes
)

type (
	// Mux is the underlying multiplexer of the I3vMux package. It is used to split
	// up a single net.Conn into multiple streams which also satisfy the net.Conn
	// interface.
	Mux struct {
		staticAead cipher.AEAD
		staticConn net.Conn
		settings   connectionSettings

		// server related fields
		pubKey  ED25519PublicKey
		privKey ED25519SecretKey

		// stream related fields
		streams        map[uint32]*Stream
		newStreams     []*Stream
		newStreamsChan chan struct{}

		// staticCloseCallback is called when the mux is closed.
		staticCloseCallback closeCallback
		// staticTimeoutCallback is called when the mux is about to time out.
		staticTimeoutCallback timeoutCallback
		staticTimeoutChanged  chan time.Duration

		// utilities
		staticDeps deps.Dependencies
		stopped    chan struct{}
		log        *persist.Logger
		mu         sync.Mutex
		wg         sync.WaitGroup
	}
	// connectionSettings contains settings related to the settings like the
	// negotiated packet size or connection timeout.
	connectionSettings struct {
		// Different connections are going to have different optimal packet sizes.
		// Typically, for IPv4-TCP connections, the optimal packet size is going to
		// be 1460 bytes, and for IPv6-TCP connections, the optimal packet size is
		// going to be 1440 bytes.
		//
		// These numbers are derived from the fact that Ethernetv2 packets generally
		// have an MTU of 1500 bytes. 20 bytes goes to IPv4 headers, 40 bytes
		// goes to IPv6 headers, and 20 bytes goes to TCP headerrs.
		//
		// The requestedPacketSize is not allowed to be smaller than 1220 bytes.
		// This is derived from the IPv6 required link MTU of 1280. 40 bytes of that
		// will be lost to IPv6, and 20 more will be lost to TCP.
		//
		RequestedPacketSize uint16

		// FrameSize establishes the size in bytes that the mux will expect for
		// an encrypted frame. If a larger frame is sent, the mux connection
		// will be closed.
		//
		// A typical maximum frame size is 64 packets. Generally frame sizes are
		// going to be small so that streams can be intertwined, and so that it is
		// easy to interrupt a low priority stream with a sudden high priority
		// stream. This value must be at least 16 packets.
		FrameSize uint32

		// MaxTimeout defines the maximum timeout that the mux peer is
		// willing to accept for the connection. Keepalives need to be sent at least
		// this often.
		MaxTimeout uint16
	}
	// closeCallback is the signature of a method which will be called when a
	// mux is closed but before the underlying connection is closed.
	closeCallback func(*Mux)
	// timeoutCallback is the signature of a method used as the timeout callback
	// of the mux. A method called before a mux is about to time out.
	timeoutCallback func(*Mux)
)

// defaultConnectionSettings are the default settings set by a mux on
// creation.
func defaultConnectionSettings(conn net.Conn) (connectionSettings, error) {
	var packetSize uint16
	// Figure out if connection's ip is IPv4 or IPv6
	host, _, err := net.SplitHostPort(conn.RemoteAddr().String())
	if err != nil {
		return connectionSettings{}, err
	}
	ip := net.ParseIP(host)
	if ip.To4() != nil {
		packetSize = 1460 // IPv4
	} else if ip.To16() != nil {
		packetSize = 1440 // IPv6
	} else {
		return connectionSettings{}, errors.New("invalid ip address")
	}
	return connectionSettings{
		RequestedPacketSize: packetSize,
		FrameSize:           uint32(64 * packetSize), // 64 packets
		MaxTimeout:          DefaultMaxTimeout,       // 20 minutes
	}, nil
}

// newMux wraps a net.Conn in a multiplexer and will immediately begin to
// upgrade the connection to an encrypted one.
func newMux(conn net.Conn, pubKey ED25519PublicKey, privKey ED25519SecretKey, log *persist.Logger, f closeCallback, t timeoutCallback) (*Mux, error) {
	connSettings, err := defaultConnectionSettings(conn)
	if err != nil {
		return nil, err
	}
	mux := &Mux{
		staticDeps: deps.ProdDependencies,
		log:        log,
		staticConn: conn,
		settings:   connSettings,
		// server related fields
		pubKey:                pubKey,
		privKey:               privKey,
		stopped:               make(chan struct{}),
		streams:               make(map[uint32]*Stream),
		staticCloseCallback:   f,
		staticTimeoutCallback: t,
		staticTimeoutChanged:  make(chan time.Duration, 1),
	}
	mux.staticTimeoutChanged <- time.Duration(connSettings.MaxTimeout) * time.Second
	mux.newStreamsChan = make(chan struct{}, 100)
	// Spawn thread to call timeout callback whenever mux is about to time out.
	mux.wg.Add(1)
	go func() {
		defer mux.wg.Done()
		mux.threadedHandleMaxTimeoutCallback()
	}()
	return mux, nil
}

// NewClientMux wraps a connection in a Mux and immediately initializes the
// handshake to establish an encrypted connection.
func NewClientMux(conn net.Conn, expectedPubKey ED25519PublicKey, log *persist.Logger, f closeCallback, t timeoutCallback) (*Mux, error) {
	// client doesn't need the pubKey and privKey since it doesn't need to sign the encryptionResponse.
	mux, err := newMux(conn, ED25519PublicKey{}, ED25519SecretKey{}, log, f, t)
	if err != nil {
		return nil, errors.AddContext(err, "failed to create new client mux")
	}
	// init client side connection
	if err := mux.initClient(expectedPubKey); err != nil {
		return nil, errors.AddContext(err, "failed to init client connection")
	}
	// start the background thread.
	mux.wg.Add(1)
	go func() {
		mux.threadedReceiveData()
		mux.wg.Done()
	}()
	return mux, nil
}

// NewServerMux wraps a connection in a Mux and starts waiting for the peer to
// start initializing the handshake to establish an encrypted connection.
func NewServerMux(conn net.Conn, pubKey ED25519PublicKey, privKey ED25519SecretKey, log *persist.Logger, f closeCallback, t timeoutCallback) (*Mux, error) {
	// server doesn't need the expectedPubKey since the client's key is ephemeral.
	mux, err := newMux(conn, pubKey, privKey, log, f, t)
	if err != nil {
		return nil, errors.AddContext(err, "failed to create new server mux")
	}
	// init server side connection
	if err := mux.initServer(); err != nil {
		return nil, errors.AddContext(err, "failed to init server connection")
	}
	// start the background thread.
	mux.wg.Add(1)
	go func() {
		mux.threadedReceiveData()
		mux.wg.Done()
	}()
	return mux, nil
}

// Keepalive sends a keepalive frame which updates the timeout on both peers.
func (m *Mux) Keepalive() error {
	return m.managedWriteKeepaliveFrame()
}

// StopChan returns the stopped channel for the caller to check if the mux has
// been closed.
func (m *Mux) StopChan() <-chan struct{} {
	return m.stopped
}

// managedClose closes the mux.
func (m *Mux) managedClose() error {
	select {
	case <-m.stopped:
		return nil
	default:
	}
	close(m.stopped)
	m.staticCloseCallback(m)
	return m.staticConn.Close()
}

// Close closes the mux and waits for background threads to finish.
func (m *Mux) Close() error {
	err := m.managedClose()
	m.wg.Wait()
	return err
}

// newFrameID returns an unused frame id. The stream with the returned id needs
// to be added to m.streams right away without unlocking the mutex in the
// meantime. Otherwise a subsequent call to this method might returns the same
// id again.
func (m *Mux) newFrameID() (id uint32) {
	id = binary.LittleEndian.Uint32(fastrand.Bytes(4))
	for _, taken := m.streams[id]; taken || id < numReservedFrameIDs; {
		id = binary.LittleEndian.Uint32(fastrand.Bytes(4))
	}
	return id
}
