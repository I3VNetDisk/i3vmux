package mux

import (
	"bytes"
	"reflect"
	"testing"

	"gitlab.com/I3VNetDisk/fastrand"
	"golang.org/x/crypto/chacha20poly1305"
)

// TestMarshalFrameSize is a test for sanity checking the expected marshaled
// size of a frame and its header. The constant can't be changed without
// breaking this test.
func TestMarshalFrameSize(t *testing.T) {
	if marshaledFrameHeaderSize != 10 {
		t.Fatalf("expected marshaledFrameHeaderSize to be %v but was %v", 10, marshaledFrameHeaderSize)
	}
	payloadSize := 100
	f := frame{
		payload: make([]byte, payloadSize),
	}
	if f.marshaledSize() != uint64(marshaledFrameHeaderSize+payloadSize) {
		t.Fatalf("expected marshaledSize of frame to be %v but was %v", marshaledFrameHeaderSize+payloadSize, f.marshaledSize())
	}
}

// TestEstablishEncryptionRequestResponseSize sanity checks whether the size of
// an encryptionRequestFrame and encryptonResponseFrame match their
// corresponding constants.
func TestEstablishEncryptionRequestResponseSize(t *testing.T) {
	_, _, reqFrame := newEstablishEncryptionRequestFrame()
	d, err := reqFrame.Marshal()
	if err != nil {
		t.Fatal(err)
	}
	if len(d) != marshaledEstablishEncryptionRequestFrameSize {
		t.Fatalf("expected len(d) to be %v but was %v", marshaledEstablishEncryptionRequestFrameSize, len(d))
	}
	_, _, respFrame := newEstablishEncryptionResponseFrame(X25519PublicKey{}, ED25519SecretKey{})
	d, err = respFrame.Marshal()
	if err != nil {
		t.Fatal(err)
	}
	if len(d) != marshaledEstablishEncryptionResponseFrameSize {
		t.Fatalf("expected len(d) to be %v but was %v", marshaledEstablishEncryptionResponseFrameSize, len(d))
	}
}

// TestMarshalUnmarshalFrame checks if a frame can be marshaled, padded and then
// unmarshaled correctly.
func TestMarshalUnmarshalFrame(t *testing.T) {
	// Create frame with random payload.
	length := fastrand.Intn(3) // [0;2]
	f := frame{
		id:      uint32(fastrand.Intn(100)),
		length:  uint32(length),
		flags:   uint16(fastrand.Intn(100)),
		payload: fastrand.Bytes(length),
	}
	// Marshal it.
	b, err := f.Marshal()
	if err != nil {
		t.Fatal(err)
	}
	// Add some random padding to the marshaled frame.
	b = append(b, fastrand.Bytes(fastrand.Intn(3))...)
	// Unmarshal it.
	var f2 frame
	if err := f2.Unmarshal(b); err != nil {
		t.Fatal(err)
	}
	// The result should match the original.
	if !reflect.DeepEqual(f, f2) {
		t.Fatal("unmarshaled frame doesn't match original")
	}
}

// TestWriteReadFrame tests the writeFrame and readFrame methods.
func TestWriteReadFrame(t *testing.T) {
	// Create a random cipher.
	key := fastrand.Bytes(X25519KeyLen)
	aead, err := chacha20poly1305.New(key[:])
	if err != nil {
		t.Fatal(err)
	}
	// Create frame with random payload.
	length := fastrand.Intn(3) // [0;2]
	f := frame{
		id:      uint32(fastrand.Intn(100)),
		length:  uint32(length),
		flags:   uint16(fastrand.Intn(100)),
		payload: fastrand.Bytes(length),
	}
	// Prepare a buffer for the encrypted frame.
	encryptedFrameSize := int(1460)
	buf := bytes.NewBuffer(make([]byte, 0, encryptedFrameSize))
	// Write the frame to the buffer.
	n, err := writeFrame(buf, aead, f, encryptedFrameSize)
	if err != nil {
		t.Fatal(err)
	}
	if n != encryptedFrameSize {
		t.Fatalf("n should be %v but was %v", encryptedFrameSize, n)
	}
	// Read the frame from the buffer.
	f2, err := readFrame(buf, aead, encryptedFrameSize)
	if err != nil {
		t.Fatal(err)
	}
	// Compare the frames.
	if !reflect.DeepEqual(f, f2) {
		t.Log(f)
		t.Log(f2)
		t.Fatal("read frame doesn't match written frame")
	}
}
