package mux

import (
	"bytes"
	"testing"

	"gitlab.com/I3VNetDisk/fastrand"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/chacha20poly1305"
)

// TestDeriveSharedSecret checks that deriveSharedSecret correctly computes the
// shared secret used to encrypt the communication between two peers.
func TestDeriveSharedSecret(t *testing.T) {
	// Create 2 key pairs.
	privKey1, pubKey1 := generateX25519KeyPair()
	privKey2, pubKey2 := generateX25519KeyPair()
	// Derive the shared key twice.
	sharedKey1 := deriveSharedSecret(privKey1, pubKey2)
	sharedKey2 := deriveSharedSecret(privKey2, pubKey1)
	// The shared keys should match.
	if !bytes.Equal(sharedKey1[:], sharedKey2[:]) {
		t.Log("privKey1", privKey1)
		t.Log("pubKey1", pubKey1)
		t.Log("privKey2", privKey2)
		t.Log("pubKey2", pubKey2)
		t.Log("sharedKey1", sharedKey1)
		t.Log("sharedKey2:", sharedKey2)
		t.Fatal("shared keys don't match")
	}
}

// TestEncryptDecryptFrame tests encryptFrame and decryptFrame. It checks if the
// encryption inputs are checked correctly, if padding is applied correctly and
// whether the decryption produces the same plaintext as before.
func TestEncryptDecryptFrame(t *testing.T) {
	// Create a random key.
	key := fastrand.Bytes(X25519KeyLen)

	// Initialize the cipher.
	aead, err := chacha20poly1305.New(key[:])
	if err != nil {
		t.Fatal(err)
	}
	// Create a frame that is maller than the encryptedFrameSize, one that is
	// the same size and one that is bigger to test for edge cases.
	encryptedFrameSize := int(1460)
	mfs := maxFrameSize(encryptedFrameSize, aead)
	smallFrame := fastrand.Bytes(mfs - 1)
	mediumFrame := fastrand.Bytes(mfs)
	largeFrame := fastrand.Bytes(mfs + 1)

	// Encrypt the frames. Should work for the small and medium frame but not the
	// large one.
	smallFrameEncrypted, err := encryptFrame(encryptedFrameSize, smallFrame, aead)
	if err != nil {
		t.Fatal(err)
	}
	mediumFrameEncrypted, err := encryptFrame(encryptedFrameSize, mediumFrame, aead)
	if err != nil {
		t.Fatal(err)
	}
	_, err = encryptFrame(encryptedFrameSize, largeFrame, aead)
	if err == nil {
		t.Fatal("encrypting the largeFrame should have failed but didn't")
	}

	// Check the size of the encrypted frames.
	if len(smallFrameEncrypted) != encryptedFrameSize {
		t.Fatalf("expected frame to have size %v but was %v", encryptedFrameSize, len(smallFrameEncrypted))
	}
	if len(mediumFrameEncrypted) != encryptedFrameSize {
		t.Fatalf("expected frame to have size %v but was %v", encryptedFrameSize, len(mediumFrameEncrypted))
	}

	// Decrypt the frames again. The decrypted frames minus padding should match
	// the original data.
	smallFrame2, err := decryptFrame(encryptedFrameSize, smallFrameEncrypted, aead)
	if err != nil {
		t.Fatal(err)
	}
	mediumFrame2, err := decryptFrame(encryptedFrameSize, mediumFrameEncrypted, aead)
	if err != nil {
		t.Fatal(err)
	}
	smallFrame2 = smallFrame2[:len(smallFrame)]
	mediumFrame2 = mediumFrame2[:len(mediumFrame)]
	if !bytes.Equal(smallFrame, smallFrame2) {
		t.Fatal("frames don't match")
	}
	if !bytes.Equal(mediumFrame, mediumFrame2) {
		t.Fatal("frames don't match")
	}
}

// TestSignVerifyHash tests if creating signatures for hashes and verifying them
// works as expected.
func TestSignVerifyHash(t *testing.T) {
	// Create some random data.
	data := fastrand.Bytes(100)
	// Generate a keypair.
	sk, pk := GenerateED25519KeyPair()
	// Hash the data.
	hash := blake2b.Sum256(data)
	// Sign the data.
	sig := signHash(hash, sk)
	// Verify signature
	if !verifyHash(hash, pk, sig) {
		t.Fatal("signature wasn't verified")
	}
}
