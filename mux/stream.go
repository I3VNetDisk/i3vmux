package mux

import (
	"io"
	"net"
	"sync"
	"time"

	"gitlab.com/I3VNetDisk/errors"
)

// ErrStreamTimedOut is returned when trying to read from or write to a timed
// out stream.
var ErrStreamTimedOut = errors.New("stream timed out")

// Stream implements a multiplexed connection to the connected peer. A Stream is
// a drop-in replacement for a net.Conn.
type Stream struct {
	readDeadline  time.Time // timeout for reading from stream
	writeDeadline time.Time // deadline for writing to stream
	mu            sync.Mutex

	staticR   *io.PipeReader // used to read data from stream
	staticW   *io.PipeWriter // used to write data to which can be read from staticR
	staticID  uint32
	staticMux *Mux
}

// managedNewStream creates a new Stream object.
func (m *Mux) newStream(id uint32) *Stream {
	r, w := io.Pipe()
	// Prepare a slice for the buffer and append the initial payload right away.
	stream := &Stream{
		readDeadline:  time.Time{}, // infinite
		writeDeadline: time.Time{}, // infinite
		staticR:       r,
		staticW:       w,
		staticID:      id,
		staticMux:     m,
	}
	m.streams[stream.staticID] = stream
	return stream
}

// NewStream creates a new outgoing stream.
func (m *Mux) NewStream() (*Stream, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	stream := m.newStream(m.newFrameID())
	return stream, nil
}

// AcceptStream listens for a new incoming stream.
func (m *Mux) AcceptStream() (*Stream, error) {
	return m.managedAcceptStream()
}

// Close implements net.Conn. It removes the stream from the mux and closes the
// underlying writer and reader.
func (s *Stream) Close() error {
	err := s.staticMux.managedWriteFinalFrame(s.staticID)
	if err != nil {
		return errors.AddContext(err, "failed to write final frame when closing stream")
	}
	_, err = s.staticMux.managedRemoveStream(s.staticID, nil)
	return err
}

// managedClose is similar to close but doesn't remove the stream from its
// parent mux. Therefore Close should usually be called.
func (s *Stream) managedClose(err error) error {
	return s.staticW.CloseWithError(err)
}

// LocalAddr implements net.Conn.
func (s *Stream) LocalAddr() net.Addr {
	return s.staticMux.staticConn.LocalAddr()
}

// Read implements net.Conn by reading from a reader which is fed by the data
// fetching background thread of the mux. If no data is available Read will
// block. If data is available and not read, other streams of the same
// connection will be blocked as well.
func (s *Stream) Read(b []byte) (n int, err error) {
	// Check deadline before reading.
	deadline := s.managedReadDeadline()
	if !deadline.IsZero() && time.Now().After(deadline) {
		return 0, ErrStreamTimedOut
	}
	// Prepare a timer for the timeout.
	timer := time.NewTimer(time.Until(s.managedReadDeadline()))
	// If the timeout is time.Time{} it's inifinite.
	cancel := timer.C
	if deadline.IsZero() {
		c := make(chan time.Time)
		defer close(c) // close c
		cancel = c
	}
	n, err = s.managedRead(b, cancel)
	// Drain timer if necessary.
	if !timer.Stop() && !errors.Contains(err, ErrStreamTimedOut) {
		<-timer.C
	}
	return
}

// RemoteAddr implements net.Conn.
func (s *Stream) RemoteAddr() net.Addr {
	return s.staticMux.staticConn.RemoteAddr()
}

// SetDeadline implements net.Conn.
func (s *Stream) SetDeadline(t time.Time) error {
	err1 := s.SetWriteDeadline(t)
	err2 := s.SetReadDeadline(t)
	return errors.Compose(err1, err2)
}

// SetPriority sets the streams priority. Streams with higher priority will be
// scheduled more often and have therefore lower latency.
// TODO: figure out how to do that
func (s *Stream) SetPriority(priority int) error {
	panic("not implemented yet")
}

// SetReadDeadline implements net.Conn.
func (s *Stream) SetReadDeadline(t time.Time) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.readDeadline = t
	return nil
}

// SetWriteDeadline implements net.Conn.
func (s *Stream) SetWriteDeadline(t time.Time) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.writeDeadline = t
	return nil
}

// Write implements net.Conn by splitting up the data into frames, encrypting
// them and sending them over the wire one-by-one. Currently there is no
// prioritization and all calls to Write fight for the same lock to send the
// data. They will only send one frame per acquired lock though.
func (s *Stream) Write(b []byte) (n int, err error) {
	// Check deadline before starting write.
	deadline := s.managedWriteDeadline()
	if !deadline.IsZero() && time.Now().After(deadline) {
		return 0, ErrStreamTimedOut
	}
	// Prepare a timer for the timeout.
	timer := time.NewTimer(time.Until(s.managedWriteDeadline()))
	// If the timeout is time.Time{} it's inifinite.
	cancel := timer.C
	if deadline.IsZero() {
		c := make(chan time.Time)
		defer close(c) // close c
		cancel = c
	}
	n, err = s.staticMux.managedWrite(b, s, cancel)
	// Drain timer if necessary.
	if !timer.Stop() && !errors.Contains(err, ErrStreamTimedOut) {
		<-timer.C
	}
	return
}

// managedReadDeadline returns the stream's read deadline.
func (s *Stream) managedReadDeadline() time.Time {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.readDeadline
}

// managedWriteDeadline returns the stream's write deadline.
func (s *Stream) managedWriteDeadline() time.Time {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.writeDeadline
}

// managedRead implements net.Conn by reading from a reader which is fed by the
// data fetching background thread of the mux. If no data is available Read will
// block. If data is available and not read, other streams of the same
// connection will be blocked as well.
func (s *Stream) managedRead(b []byte, timeout <-chan time.Time) (n int, err error) {
	done := make(chan struct{})
	go func() {
		defer close(done)
		n, err = s.staticR.Read(b)
	}()
	// Check if we timed out or not.
	select {
	case <-timeout:
		return 0, ErrStreamTimedOut
	case <-done:
	}
	return
}
