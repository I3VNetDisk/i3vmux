package mux

import "time"

var (
	// TimeoutNotificationBuffer is the time before the maxTimeout is reached
	// when the mux calls the registered timeout callback method.
	TimeoutNotificationBuffer = 5 * time.Minute // 5 minutes / 25% of the default maxTimeout
)

// managedUpdateDeadline handles extending the timeout of the mux's underlying
// connection whenever a frame was read or written.
func (m *Mux) managedUpdateDeadline() {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.updateDeadline()
}

// updateDeadline handles extending the timeout of the mux's underlying
// connection whenever a frame was read or written.
func (m *Mux) updateDeadline() error {
	// Compute the duration until the next deadline.
	duration := time.Second * time.Duration(m.settings.MaxTimeout)
	// Set the deadline on the connection.
	err := m.staticConn.SetDeadline(time.Now().Add(duration))
	if err != nil {
		return err
	}

	// The deadline for calling the callback should be TimeoutNotificationBuffer
	// before that.
	if duration >= TimeoutNotificationBuffer {
		duration -= TimeoutNotificationBuffer
	} else {
		duration = 0
	}
	m.staticTimeoutChanged <- duration
	return nil
}

// threadedHandleMaxTimeoutCallback calls the timeout callback of the mux every
// time the mux is about to time out.
func (m *Mux) threadedHandleMaxTimeoutCallback() {
	duration := <-m.staticTimeoutChanged
	for {
		timer := time.NewTimer(duration)
		select {
		case <-m.stopped:
			return // mux was closed
		case duration = <-m.staticTimeoutChanged:
			if !timer.Stop() {
				<-timer.C // drain timer
			}
			continue // timeout changed, don't fire callback
		case <-timer.C:
		}
		// timeout reached, fire callback
		m.staticTimeoutCallback(m)

		// Before starting a new timer we wait for the timeout to be updated
		// again.
		select {
		case <-m.stopped:
			return // mux was closed
		case duration = <-m.staticTimeoutChanged:
		}
	}
}
