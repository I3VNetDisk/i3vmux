package mux

import (
	"crypto/cipher"
	"fmt"

	"golang.org/x/crypto/chacha20poly1305"

	"golang.org/x/crypto/blake2b"

	"gitlab.com/I3VNetDisk/fastrand"
	"golang.org/x/crypto/curve25519"
	"golang.org/x/crypto/ed25519"
)

const (
	// X25519KeyLen is the length of a X25519 key and as a result the length of the
	// shared key.
	X25519KeyLen = 32
	// ED25519SecretKeyLen is the length of the private key used to create the
	// signature.
	ED25519SecretKeyLen = ed25519.PrivateKeySize
	// ED25519PublicKeyLen is the length of the public key used to verify
	// signatures.
	ED25519PublicKeyLen = ed25519.PublicKeySize
	// HashSize is the size of a blake2b 256bit hash.
	HashSize = blake2b.Size256
	// SignatureSize is the size of a ed25519 signature.
	SignatureSize = ed25519.SignatureSize
)

// maxFrameSize is the maximum size a frame can have to still be below
// encryptedFrameSize after encryption.
func maxFrameSize(encryptedFrameSize int, aead cipher.AEAD) int {
	return encryptedFrameSize - aead.Overhead() - aead.NonceSize()
}

type (
	// CipherSpecifier is a specifier used to identify the cipher being used for
	// encryption.
	CipherSpecifier [16]byte
	// Hash is the result of hashing data in the i3vmux protocol.
	Hash [HashSize]byte
	// Signature is the type of a ed25519 signature.
	Signature [SignatureSize]byte
	// An X25519SecretKey is the secret half of an X25519 key pair.
	X25519SecretKey [X25519KeyLen]byte
	// An X25519PublicKey is the public half of an X25519 key pair.
	X25519PublicKey [X25519KeyLen]byte
	// An ED25519SecretKey is the private half of an ED25519 key pair.
	ED25519SecretKey [ED25519SecretKeyLen]byte
	// An ED25519PublicKey is the public half of an ED25519 key pair.
	ED25519PublicKey [ED25519PublicKeyLen]byte
)

var (
	// CipherSpecifierChaCha20Poly1305 is the specifier for the chacha20poly1305
	// aead cipher.
	CipherSpecifierChaCha20Poly1305 = CipherSpecifier{'C', 'h', 'a', 'c', 'h', 'a', '2', '0', 'P', '1', '3', '0', '5'}
)

type (
	// establishEncryptionRequest is the request sent by the client a.k.a. the
	// peer establishing the connection.
	establishEncryptionRequest struct {
		// PublicKey is the ephemeral public key of the client.
		PublicKey X25519PublicKey
		// Ciphers supported by the client.
		Ciphers []CipherSpecifier
	}
	// establishEncryptionResponse is the response sent by the server a.k.a the
	// peer being connected to.
	establishEncryptionResponse struct {
		// PublicKey is the server's ephemeral public key which is used to
		// derive the shared secret.
		PublicKey X25519PublicKey
		// Signature of (Server's ephemeral Public key | Client's Ephemeral
		// Public Key) signed with the server's non-ephemeral private key. This
		// is used to authenticate the server if the server's non-ephemeral
		// public key was known beforehand.
		Signature Signature
		// Cipher selected by the server. Must be one of the ciphers offered in
		// the establishEncryptionRequest.
		Cipher CipherSpecifier
	}
)

// createSignaturHash takes the public key of the encryptionRequest and the
// public key of the encryptionResponse and hashes them together.
func createSignatureHash(responsePubKey, requestPubKey X25519PublicKey) Hash {
	return blake2b.Sum256(append(responsePubKey[:], requestPubKey[:]...))
}

// deriveSharedSecret derives 32 bytes of entropy from a secret key and public
// key. Derivation is via ScalarMult of the private and public keys.
func deriveSharedSecret(xsk X25519SecretKey, xpk X25519PublicKey) (secret [X25519KeyLen]byte) {
	curve25519.ScalarMult(&secret, (*[X25519KeyLen]byte)(&xsk), (*[X25519KeyLen]byte)(&xpk))
	return
}

// generateX25519KeyPair generates an ephemeral key pair for use in ECDH.
func generateX25519KeyPair() (xsk X25519SecretKey, xpk X25519PublicKey) {
	fastrand.Read(xsk[:])
	curve25519.ScalarBaseMult((*[X25519KeyLen]byte)(&xpk), (*[X25519KeyLen]byte)(&xsk))
	return
}

// GenerateED25519KeyPair creates a public-secret keypair that can be used to
// sign and verify messages.
func GenerateED25519KeyPair() (sk ED25519SecretKey, pk ED25519PublicKey) {
	// no error possible when using fastrand.Reader
	epk, esk, _ := ed25519.GenerateKey(fastrand.Reader)
	copy(sk[:], esk)
	copy(pk[:], epk)
	return
}

// encryptFrame will encrypt a raw frame. The resulting ciphertext will always
// have encryptedFrameSize.
func encryptFrame(encryptedFrameSize int, frame []byte, aead cipher.AEAD) ([]byte, error) {
	maxFrameSize := maxFrameSize(encryptedFrameSize, aead)
	if len(frame) > maxFrameSize {
		return nil, fmt.Errorf("frame is too big and should be split up in multiple frames: %v > %v",
			len(frame), maxFrameSize)
	}
	if len(frame) < maxFrameSize {
		frame = append(frame, fastrand.Bytes(maxFrameSize-len(frame))...)
	}
	nonce := fastrand.Bytes(aead.NonceSize())
	return aead.Seal(nonce, nonce, frame, nil), nil // reuse nonce memory by passing it in as dst too
}

// decryptFrame will decrypt an encrypted frame.
func decryptFrame(encryptedFrameSize int, frame []byte, aead cipher.AEAD) ([]byte, error) {
	// sanity check length of encrypted frame.
	if len(frame) != encryptedFrameSize {
		return nil, fmt.Errorf("encrypted frame should have size %v but had %v", encryptedFrameSize, len(frame))
	}
	nonce, ciphertext := frame[:aead.NonceSize()], frame[aead.NonceSize():]
	return aead.Open(nil, nonce, ciphertext, nil)
}

// initCipher creates an AEAD from a cipher specifier and key. If the specifier
// is unknown, an error is returned.
func initCipher(key []byte, cipher CipherSpecifier) (cipher.AEAD, error) {
	switch cipher {
	case CipherSpecifierChaCha20Poly1305:
		return chacha20poly1305.New(key)
	default:
	}
	return nil, fmt.Errorf("unknown cipher %v", cipher)
}

// signHash signs a message using a secret key.
func signHash(data Hash, sk ED25519SecretKey) (sig Signature) {
	copy(sig[:], ed25519.Sign(sk[:], data[:]))
	return
}

// verifyHash uses a public key and input data to verify a signature.
func verifyHash(data Hash, pk ED25519PublicKey, sig Signature) bool {
	return ed25519.Verify(pk[:], data[:], sig[:])
}
