package mux

import (
	"crypto/cipher"
	"encoding/binary"
	"fmt"
	"io"
	"net"

	"gitlab.com/I3VNetDisk/I3v/encoding"
	"gitlab.com/I3VNetDisk/errors"
)

// TODO (followup): extend writeFrame and readFrame to support covert frames.

const (
	// marshaledHeaderSize returns the number of bytes it takes to marshal a frame
	// without its payload. This is the same size for every frame.
	marshaledFrameHeaderSize = 4 + 4 + 2
	// marshaledEstablishEncryptionRequestFrameSize is the number of bytes that
	// a frame with an establishEncryptonRequest in the payload has.
	marshaledEstablishEncryptionRequestFrameSize = marshaledFrameHeaderSize + 56
	// marshaledEstablishEncryptionResponseFrameSize is the number of bytes that
	// a frame with an establishEncryptonResponse in the payload has.
	marshaledEstablishEncryptionResponseFrameSize = marshaledFrameHeaderSize + 112
)

// Flag bits and what they mean.
const (
	// frameBitFinalFrame indicates whether or not this is the final frame for
	// this stream. If set, the stream is expected to be closed with no
	// response.
	frameBitFinalFrame = 1 << 0
	// frameBitErrorFrame indicates whether the stream is being closed because
	// of a I3vMux error. If set, the entire payload is an error string. Bit 0
	// must be set if bit 1 is set.
	frameBitErrorFrame = 1 << 1
	// frameBitPaddingContainsAnotherFrame indicates that another frame exists
	// in the padding of the current frame. This is an optimization technique to
	// increase the frame density when every frame needs to be padded to an
	// exact number of packets.
	frameBitPaddingContainsAnotherFrame = 1 << 2
)

// The first 256 reserved IDs and their meanings.
const (
	// frameIDErrorBadInit indicates that the frame ID was initialized
	// improperly, using the empty value instead of setting the frame to a
	// correct value. Any time the peermux receives a frameErrorBadInit, an
	// error frame will be returned to the peer indicating that a bad frame was
	// sent.
	frameIDErrorBadInit = iota
	// frameIDEstablishEncryption is used to indicate that a frame contains
	// setup information to establish a connection between two I3vMux peers. If
	// setup has not yet been completed, this is the only frame ID that is
	// allowed.
	frameIDEstablishEncryption
	// frameIDUpdateSettings is used to indicate that a peer wants to update
	// their connection settings. This is the first frame that is sent after
	// establishing an encrypted connection.
	frameIDUpdateSettings
	// frameIDKeepalive indicates that the peer is sending this frame to reset
	// the timeout on the I3vMux connection. Keepalive frames only need to be
	// sent if there has been no other recent activity - all frames will reset
	// the keepalive.
	frameIDKeepalive
	// frameIDNewStream announces the creation of a new stream from the other
	// party. This frame is used to create new multiplexed connections across
	// the I3vMux and will not only contain the announcement of a new stream but
	// also the first bits of data in the stream.
	frameIDNewStream
)

// frame defines a single frame for sending data through the peermux session.
// Each frame has 10 bytes of overhead plus the potential AEAD overhead after
// encryption. Typically, this will result in substantially less than 1%
// overhead for high performance transfers.
type frame struct {
	// The id of the frame indicates what stream the frame is contributing to. A
	// set of reserved IDs are used for I3vMux communications.
	id uint32

	// length indicates the number of bytes in the payload of the frame. All
	// frames are padded so that they consume an exact number of packets.
	length uint32

	// flags describes 16 flags that can be set to indicate information about
	// the frame and optimizations within the frame.
	flags uint16

	// The payload that is intended to be forwarded to the underlying stream.
	payload []byte
}

// marshaledSize returns the number of bytes it would take to marshal a specific
// frame including the payload.
func (f *frame) marshaledSize() uint64 {
	return marshaledFrameHeaderSize + uint64(len(f.payload))
}

// Marshal marshals a frame into a byte slice.
func (f frame) Marshal() ([]byte, error) {
	d := make([]byte, f.marshaledSize())
	binary.LittleEndian.PutUint32(d[:4], f.id)
	binary.LittleEndian.PutUint32(d[4:8], f.length)
	binary.LittleEndian.PutUint16(d[8:10], f.flags)
	n := copy(d[10:], f.payload)
	if uint32(n) != f.length || n != len(f.payload) {
		return nil, fmt.Errorf("failed to marshal frame due to a length/payload mismatch %v %v %v",
			f.length, len(f.payload), n)
	}
	return d, nil
}

// Unmarshal unmarshals a byte slice into a frame.
func (f *frame) Unmarshal(d []byte) error {
	if len(d) < marshaledFrameHeaderSize {
		return errors.New("failed to unmarshal frame due to not enough data being available")
	}
	f.id = binary.LittleEndian.Uint32(d[:4])
	f.length = binary.LittleEndian.Uint32(d[4:8])
	f.flags = binary.LittleEndian.Uint16(d[8:10])
	if remainingBytes := uint32(len(d[10:])); remainingBytes < f.length {
		return fmt.Errorf("length is %v but remaining payload has length %v", f.length, remainingBytes)
	}
	f.payload = d[10:][:f.length]
	return nil
}

// writeFrame encrypts a frame, adds padding if necessary and finally writes it
// to the provided io.Writer.
func writeFrame(w io.Writer, aead cipher.AEAD, f frame, encryptedFrameSize int) (int, error) {
	b, err := f.Marshal()
	if err != nil {
		return 0, errors.AddContext(err, "failed to marshal frame")
	}
	encryptedFrame, err := encryptFrame(encryptedFrameSize, b, aead)
	if err != nil {
		return 0, errors.AddContext(err, "failed to encrypt frame")
	}
	return w.Write(encryptedFrame)
}

// readFrame reads a frame from the provided io.Reader, decrypts it and finally
// unmarshals it.
func readFrame(r io.Reader, aead cipher.AEAD, encryptedFrameSize int) (unmarshaledFrame frame, _ error) {
	encryptedFrame := make([]byte, encryptedFrameSize)
	_, err := io.ReadFull(r, encryptedFrame)
	if err, ok := err.(net.Error); ok && err.Timeout() {
		return frame{}, err // don't wrap timeout error
	}
	if err != nil {
		return frame{}, errors.AddContext(err, "failed to read encrypted frame")
	}
	decryptedFrame, err := decryptFrame(encryptedFrameSize, encryptedFrame, aead)
	if err != nil {
		return frame{}, errors.AddContext(err, "failed to decrypt frame")
	}
	if err := unmarshaledFrame.Unmarshal(decryptedFrame); err != nil {
		return frame{}, errors.AddContext(err, "failed to unmarshal frame header")
	}
	return unmarshaledFrame, nil
}

// newEstablishEncryptionRequestFrame creates a new frame that can be sent over
// the wire to establish an encrypted stream. It generates a new ephemeral key
// pair every time it is called.
func newEstablishEncryptionRequestFrame() (X25519SecretKey, X25519PublicKey, frame) {
	// Prepare request.
	xsk, xpk := generateX25519KeyPair()
	req := establishEncryptionRequest{
		Ciphers:   []CipherSpecifier{CipherSpecifierChaCha20Poly1305},
		PublicKey: xpk,
	}
	// Prepare frame.
	payload := encoding.Marshal(req)
	f := frame{
		id:      frameIDEstablishEncryption,
		length:  uint32(len(payload)),
		flags:   0,
		payload: payload,
	}
	return xsk, xpk, f
}

// newEstablishEncryptionResponseFrame creates a new frame that can be sent over
// the wire to establish an encrypted stream. It generates a new ephemeral key
// pair every time it is called.
func newEstablishEncryptionResponseFrame(requestPubKey X25519PublicKey, serverPrivKey ED25519SecretKey) (X25519SecretKey, CipherSpecifier, frame) {
	// Create signature.
	xsk, xpk := generateX25519KeyPair()
	hash := createSignatureHash(xpk, requestPubKey)
	sig := signHash(hash, serverPrivKey)
	// Prepare response.
	resp := establishEncryptionResponse{
		Cipher:    CipherSpecifierChaCha20Poly1305,
		PublicKey: xpk,
		Signature: sig,
	}
	// Prepare frame.
	payload := encoding.Marshal(resp)
	f := frame{
		id:      frameIDEstablishEncryption,
		length:  uint32(len(payload)),
		flags:   0,
		payload: payload,
	}
	return xsk, resp.Cipher, f
}

// newUpdateConnectionSettingsFrame creates a new frame containing updated
// connection settings.
func newUpdateConnectionSettingsFrame(settings connectionSettings) frame {
	// Prepare frame.
	payload := encoding.Marshal(settings)
	f := frame{
		id:      frameIDUpdateSettings,
		length:  uint32(len(payload)),
		flags:   0,
		payload: payload,
	}
	return f
}

// newPayloadFrame creates a new frame with the given id and payload.
func newPayloadFrame(id uint32, payload []byte) frame {
	return frame{
		id:      id,
		length:  uint32(len(payload)),
		flags:   0,
		payload: payload,
	}
}

// newErrorFrame creates a new frame with the given id and the given error as
// payload.
func newErrorFrame(id uint32, err error) frame {
	f := newPayloadFrame(id, []byte(err.Error()))
	f.flags = frameBitErrorFrame | frameBitFinalFrame
	return f
}

// newKeepaliveFrame is an empty payload frame with the frameIDKeepalive ID.
func newKeepaliveFrame() frame {
	f := newPayloadFrame(frameIDKeepalive, []byte{})
	return f
}

// newFinalFrame creates a new frame with the given id and the correct flags
// set.
func newFinalFrame(id uint32) frame {
	f := newPayloadFrame(id, nil)
	f.flags = frameBitFinalFrame
	return f
}
